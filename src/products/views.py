from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from .models import Product
from django.http import Http404
# Create your views here.



class ProductFeaturedListView(ListView):
    template_name="products/list.html"

    def get_queryset(self, *args, **kwargs):
        request=self.request
        return Product.objects.featured()



class ProductFeaturedDetialView(DetailView):
    template_name="products/featured-details.html"

    def get_queryset(self, *args, **kwargs):
        request=self.request
        return Product.objects.featured()




class ProductListView(ListView):
    # queryset = Product.objects.all()
    template_name = "products/list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        print("********** I want to see everything: ***********"+str(context))
        return context

    def get_queryset(self, *args, **kwargs):
        return Product.objects.all()

def product_list_view(request):
    queryset = Product.objects.all()
    context ={
        "object_list":queryset,
    }
    return render(request, "products/list.html",context)


class ProductDetailView(DetailView):
    # queryset = Product.objects.all()
    template_name = "products/details.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        print("********** Where is url path: ***********"+str(context))
        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        pk = self.kwargs.get('pk')
        instance = Product.objects.get_by_id(pk)
        if instance is None:
            raise Http404("Product doesn't exist")
        return instance

    # def get_queryset(self, *args, **kwargs):
    #     request = self.request
    #     pk = self.kwargs.get('pk')
    #     return Product.objects.filter(pk=pk)



def product_details_view(request, pk=None, *args, **kwargs):
    # instance = Product.objects.get(id=pk)

    # 1st way to show 404
    # instance = get_object_or_404(Product, pk=pk)

    # 2nd way to show 404
    # try:
    #     instance= Product.objects.get(id=pk)
    # except Product.DoesNotExist:
    #     print("No product here.")
    #     raise Http404("Product doesn't exist")
    # except:
    #     print("Huh?")


    # #3rd way to show 404
    # qs = Product.objects.filter(id=pk)
    # print(qs)
    # if qs.exists() and qs.count() == 1:
    #     instance = qs.first()
    # else:
    #     raise Http404("Product doesn't exist")

    #custom Model Manager
    instance = Product.objects.get_by_id(pk)
    print(instance)

    if instance is None:
        raise Http404("Product doesn't exist")

    context ={
        "object":instance,
    }



    return render(request, "products/details.html",context)
