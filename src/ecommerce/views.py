from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import ContactForm, LoginForm, RegisterForm
from django.contrib.auth import authenticate, login, get_user_model

def home_page(request):
    context = {
        "title": "Hello World!!!",
        "content": "Welcome to the Home Page...",
    }

    if request.user.is_authenticated():
        print("System: "+str(request.user.is_authenticated()))
        context["premium_content"]="Yeahhhhh..."
    return render(request, "home_page.html", context)


def about_page(request):
    context = {
        "title":"About Page",
        "content": "Welcome to the About Page...",
    }
    return render(request, "home_page.html", context)


def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        "title": "Contact Page",
        "content": "Welcome to the Contact Page...",
        "form": contact_form,
    }

    if contact_form.is_valid():
        print(contact_form.cleaned_data)

    return render(request, "contact/view.html", context)


def login_page(request):
    form = LoginForm(request.POST or None)
    context = {
        "form":form
    }
    print("User Login: "+str(request.user.is_authenticated()))

    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request,username=username, password=password)
        # print("User Login: "+str(request.user.is_authenticated()))
        print(user)
        if user is not None:
            # print("User Login: "+str(request.user.is_authenticated()))
            login(request,user)
            # context["form"] = LoginForm()
            return redirect("/")
        else:
            print("Error")
    return render(request, "auth/login.html", context)


User = get_user_model()
def register_page(request):
    form = RegisterForm(request.POST or None)
    context = {
        "form":form
    }
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password")
        # confirm_password = form.cleaned_data.get("password2")
        new_user = User.objects.create_user(username, email, password)
        # print(form.cleaned_data)
        print(new_user)
    return render(request, "auth/register.html", context)
